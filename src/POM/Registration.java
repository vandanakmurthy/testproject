package POM;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import generic.Base_Page;


public class Registration extends Base_Page
{
	@FindBy(xpath="/html/body/div[4]/div/div/div/div/div/form/fieldset[1]/input")
	private WebElement name;
	
	@FindBy(xpath="/html/body/div[4]/div/div/div/div/div/form/fieldset[2]/input")
	private WebElement phone;
	
	@FindBy(xpath = "/html/body/div[4]/div/div/div/div/div/form/fieldset[3]/input")
	private WebElement email;
	
	@FindBy(xpath = "//*[@id=\"load_form\"]/fieldset[4]/select")
	private WebElement country;
	
	@FindBy(xpath="//*[@id=\"load_form\"]/fieldset[5]/input")
	private WebElement city;
	
	@FindBy(xpath="//*[@id=\"load_form\"]/fieldset[6]/input")
	private WebElement user;
	
	@FindBy(xpath="//*[@id=\"load_form\"]/fieldset[7]/input")
    private WebElement password;
	
	@FindBy(xpath="/html/body/div[4]/div/div/div/div/div/form/div/div[2]/input")
	private WebElement submit;

public Registration(WebDriver driver)
{
	super(driver);
	PageFactory.initElements(driver, this);
}

public void setusername(String name1)
{
	name.sendKeys(name1);
}
public void setphone(String phone1)
{
	phone.sendKeys(phone1);
}
public void setemail(String email1)
{
	email.sendKeys(email1);
}
public void setcountry(String country1)
{
	country.sendKeys(country1);
}
public void setcity(String city1)
{
	city.sendKeys(city1);
}
public void setuser(String user1)
{
	user.sendKeys(user1);
}

public void setpassword(String password1)
{
	password.sendKeys(password1);
}

public void clicksubmit()
{
	submit.click();
}
}

